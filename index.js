const koa = require('koa')
const routes = require('koa-route')
const session = require('koa-generic-session')

// const utils = require('./utils')
const auth = require('./routes/auth.js')
const userRoutes = require('./routes/user.js')
const groupRoutes = require('./routes/group.js')
// const groupUserRoutes = require('./routes/group-user.js')

const app = module.exports = koa()

// app.use(utils.logger)
// app.use(auth.initialize())

// app.use(routes.get('/auth/github',
//   auth.authenticate('github')
// ))
// app.use(routes.get('/auth/github/callback',
//   auth.authenticate('github', {
//     successRedirect: '/home',
//     failureRedirect: '/'
//   })
// ))

app.keys = ['i m secret']
app.use(session())
app.use(auth)

// routes:
// User Routes
app.use(routes.post('/user', userRoutes.add))
app.use(routes.get('/user/:id', userRoutes.get))
app.use(routes.put('/user/:id', userRoutes.update))
app.use(routes.del('/user/:id', userRoutes.remove))

// // Group Routes
app.use(routes.post('/group', groupRoutes.add))
app.use(routes.get('/group/:name', groupRoutes.get))
app.use(routes.get('/group/:name/top', groupRoutes.top))
app.use(routes.put('/group/:name', groupRoutes.update))
app.use(routes.del('/group/:name', groupRoutes.remove))

// // Group User Routes
// app.use(routes.post('/group/:group-name/users'), groupUserRoutes.add)
// app.use(routes.post('/group/:group-name/users/:id'), groupUserRoutes.get)
// app.use(routes.post('/group/:group-name/users/:id'), groupUserRoutes.update)
// app.use(routes.post('/group/:group-name/users/:id'), groupUserRoutes.remove)

app.listen(4000)
console.log('http://localhost:4000')
