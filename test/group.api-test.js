/* eslint-env mocha */

const supertest = require('supertest')
const assert = require('assert')
const should = require('should')
const co = require('co')

const app = require('../index.js')
const utils = require('../utils')

const groups = utils.groups
const users = utils.users

const request = supertest.agent(app.listen())
// const users = utils.group

describe('Group Api', function () {
  const testGroup = {
    name: 'TestGroup',
    users: []
  }
  const garbageGroup = {
    name: 'Garbage',
    garbage: 'hello'
  }

  afterEach(function (done) {
    co(function * () {
      yield groups.remove({})
      yield users.remove({})
      // and other things we need to clean up
      done()
    })
  })

  it('POST /group - should create a group',
    function (done) {
      request
        .post('/group')
        .send(testGroup)
        .expect('location', /^TestGroup$/)
        .expect(201)
        .end(() => {
          co(function *() {
            const groupFromDb = yield groups.findOne({
              name: testGroup.name
            })
            should.equal(groupFromDb.name, testGroup.name)
          }).then(done, done)
        })
    })

  it('POST /group - should filter garbage',
      function (done) {
        request
          .post('/group')
          .send(garbageGroup)
          .expect('location', /^Garbage$/)
          .expect(201)
          .end(() => {
            co(function *() {
              const groupFromDb = yield groups.findOne({
                name: garbageGroup.name
              })
              assert(!groupFromDb.garbage)
            }).then(done, done)
          })
      })

  it('POST /group - should keep the name unique ',
    function (done) {
      co(function * () {
        yield groups.insert({ name: 'TestGroup' })

        request
          .post('/group')
          .send(testGroup)
          .expect(405)
      }).then(done, done)
    })

  it('GET /group/:name - should get a group by name',
    function (done) {
      co(function * () {
        yield groups.insert(testGroup)

        request
          .get(`/group/${testGroup.name}`)
          .set('Accept', 'application/json')
          .expect('Content-Type', /json/)
          .expect(/TestGroup/)
          .expect(200, done)
      })
    })

  it('PUT /group/:name/ - should update a group',
    function (done) {
      co(function * () {
        const group = yield groups.insert(testGroup)

        request
          .put(`/group/${group.name}`)
          .send({
            winners: [
              {
                term: 'Fall 2016',
                user: 'isthisnagee'
              }
            ]
          })
        .set('Accpet', 'application/json')
        .expect(/isthisnagee/)
        .expect(200, done)
      })
    })

  it('PUT /group/:name/ - update doesn\'t add garbage',
      function (done) {
        co(function * () {
          const group = yield groups.insert(testGroup)

          request
          .put(`/group/${group.name}`)
          .send({
            winners: [
              {
                term: 'Fall 2016',
                user: 'isthisnagee'
              }
            ],
            losers: 'loserz'
          })
          .set('Accpet', 'application/json')
            .expect(/((?!(loserz)).)*/)
            .expect(200, done)
        })
      })

  it('DELETE /group/:name - should delete a group',
      function (done) {
        co(function *() {
          // Insert test user in database
          const group = yield groups.insert(testGroup)
          request
            .del(`/user/${group.name}`)
            .expect(200, done)
        })
      })

  it('GET /group/:name/top - should get the user with most missed classes',
    function (done) {
      co(function * () {
        let u = [
          {
            name: 'Bill',
            username: 'hello',
            numMissedCourses: 10
          },
          {
            name: 'Joe',
            username: 'noway',
            numMissedCourses: 9
          },
          {
            name: 'Glen',
            username: 'glen',
            numMissedCourses: 11
          }
        ]
        let usernames = []
        for (let i = 0; i < u.length; i++) {
          let user = yield users.insert(u[i])
          usernames.push(user.username)
        }
        const newGroup = {
          name: 'userz',
          users: usernames
        }

        yield groups.insert(newGroup)

        request
          .get(`/group/${newGroup.name}/top`)
          .set('Accept', 'application/json')
          .expect('Content-Type', /json/)
          .expect(/Joe/)
          .expect(200, done)
      })
    })
})
