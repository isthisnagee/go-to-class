/* eslint-env mocha */

const supertest = require('supertest')
const assert = require('assert')
const should = require('should')
const co = require('co')

const app = require('../index.js')
const utils = require('../utils')

const request = supertest.agent(app.listen())
const users = utils.users

describe('User Api', function () {
  const testUser = {
    name: 'User 1',
    username: 'user1'
  }
  const garbageUser = {
    name: 'User 2',
    username: 'user2',
    hello: 'world'
  }

  // flush db after test
  afterEach(function (done) {
    co(function * () {
      yield users.remove({})
      // and other things we need to clean up
      done()
    })
  })

  it('POST /user - add a user',
    function (done) {
      request
        .post('/user')
        .send(testUser)
        .expect('location', /^user1$/) // a mongo id
        .expect(201)
        .end(() => {
          co(function *() {
            const userFromDb = yield users.findOne({ username: testUser.username })
            userFromDb.name.should.equal(testUser.name)
          }).then(done, done)
        })
    })
  it('POST /user - should filter out garbage',
    function (done) {
      request
        .post('/user')
        .send(garbageUser)
        .expect('location', /^\/user2$/) // Mongo Object Id /234234523562512512
        .expect(201)
        .end(() => {
          co(function *() {
            const userFromDb = yield users.findOne({ username: garbageUser.username })
            userFromDb.name.should.equal(garbageUser.name)
            assert(!userFromDb.hello)
            // userFromDb.hello.should.be.undefined
          }).then(done, done)
        })
    })

  it('POST /user - username should be unique',
      function (done) {
        co(function * () {
          yield users.insert(testUser)

          request
            .post('/user')
            .send({ name: 'ok go', username: 'user1' })
            .expect(405)
        }).then(done, done)
      })

  it('GET /user/:username - get a user',
    function (done) {
      co(function *() {
        var user = yield users.insert(testUser)

        request
          .get(`/user/${user.username}`)
          .set('Accept', 'application/json')
          .expect('Content-Type', /json/)
          .expect(/User 1/)
          .expect(200, done)
      })
    })

  it('PUT /user/:username - edits a user',
    function (done) {
      co(function *() {
        const user = yield users.insert(testUser)

        request
          .put(`/user/${user.username}`)
          .send({
            courses: [
              {
                name: 'CSC107'
              }
            ]
          })
          .set('Accept', 'application/json')
          .expect('Content-Type', /json/)
          .expect(/CSC107/)
          .expect(200, done)
      })
    })

  it('PUT /user/:username - should filter out garbage',
    function (done) {
      co(function *() {
        const user = yield users.insert(testUser)

        request
          .put(`/user/${user.username}`)
          .send({
            courses: [
              {
                name: 'CSC107'
              }
            ],
            hello: 'world'
          })
          .set('Accept', 'application/json')
          .expect('Content-Type', /json/)
          .expect(/CSC107/)
          .expect(/((?!(hello)).)*/)
          .expect(200, done)
      })
    })

  it('DELETE /user/:username - should remove the user',
    function (done) {
      co(function *() {
        // Insert test user in database
        const user = yield users.insert(testUser)
        // Delete the user
        request
          .del(`/user/${user.username}`)
          .expect(200, done)
      })
    })
})

