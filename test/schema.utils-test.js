/* eslint-env mocha */

const should = require('should')

const utils = require('../utils')

const schema = utils.schema

describe('Schema Type Checking', function () {
  it('should return false on empty objects',
    function () {
      (utils.followsSchema(schema.user, {}))
        .should.be.false()
    })

  it('should return false on empty schemas',
    function () {
      (utils.followsSchema({}, {test: false}))
        .should.be.false()
    })

  it('should work on number types',
    function () {
      const trueSchema = {
        hello: 'number'
      }
      const obj = {
        hello: 2
      }

      utils.followsSchema(trueSchema, obj).should.be.true()
    })

  it('should fail when expectung number and getting something else',
    function () {
      const trueSchema = {
        hello: 'number'
      }
      const obj = {
        hello: '2'
      }

      utils.followsSchema(trueSchema, obj).should.be.false()
    })

  it('should work on string types',
    function () {
      const trueSchema = {
        hello: 'string'
      }
      const obj = {
        hello: 'world'
      }
      utils.followsSchema(trueSchema, obj).should.be.true()
    })

  it('should fail when expecting a string and getting something else',
    function () {
      const trueSchema = {
        hello: 'string'
      }
      const obj = {
        hello: new Date()
      }
      utils.followsSchema(trueSchema, obj).should.be.false()
    })

  it('should check regular expressions for strings (when given)',
    function () {
      const falseSchema = {
        course: ['string', /^[A-Z]{3}$/]
      }
      const trueSchema = {
        course: ['string', /^[A-Z]{3}[0-9]{3}$/]
      }
      const obj = {
        course: 'CSC108'
      }

      const trueTest = utils.followsSchema(trueSchema, obj).should.be.true()
      const falseTest = utils.followsSchema(falseSchema, obj).should.be.false()

      trueTest && falseTest
    })

  it('should fail when passing a non-regex to a string in schema',
    function () {
      const badSchema = {
        course: ['string', 10]
      }
      const obj = {
        course: 'CSC108'
      }

      utils.followsSchema(badSchema, obj).should.be.false()
    })

  it('should check class membership',
    function () {
      const trueSchema = {
        date: ['class', Date]
      }
      const obj = {
        date: new Date()
      }

      utils.followsSchema(trueSchema, obj).should.be.true()
    })

  it('should fail when expecting a class and getting something else',
    function () {
      const trueSchema = {
        date: ['class', Date]
      }
      const obj = {
        date: 10
      }

      utils.followsSchema(trueSchema, obj).should.be.false()
    })

  it('should work on an array of numbers',
    function () {
      const trueSchema = {
        daysofWeek: ['array', 'number']
      }
      const obj = {
        daysofWeek: [0, 1, 2, 3, 4, 5, 6]
      }

      utils.followsSchema(trueSchema, obj).should.be.true()
    })

  it('should fail when expecting numbers[] and doesnt get that',
    function () {
      const trueSchema = {
        daysofWeek: ['array', 'number']
      }
      const obj = {
        daysofWeek: [0, 1, 2, 3, '4', 5, 6]
      }

      utils.followsSchema(trueSchema, obj).should.be.false()
    })

  it('should work on an array of a class',
    function () {
      const schema = {
        dates: ['array', ['class', Date]]
      }
      const obj = {
        dates: [new Date(), new Date(), new Date()]
      }
      utils.followsSchema(schema, obj).should.be.true()
    })

  it('should fail when expecting a class[] and getting mixed',
    function () {
      const schema = {
        dates: ['array', ['class', Date]]
      }
      const obj = {
        dates: [new Date(), 10, new Date()]
      }
      utils.followsSchema(schema, obj).should.be.false()
    })

  it('should check an array of strings that satisfy a regexp',
    function () {
      const trueSchema = {
        courses: ['array', ['string', /^[A-Z]{3}[0-9]{3}$/]]
      }
      const obj = {
        courses: ['CSC109', 'CSC108']
      }

      utils.followsSchema(trueSchema, obj).should.be.true()
    })

  it('should check nested types/schemas',
    function () {
      const nested = {
        name: ['string', /^[A-Z]{3}[0-9]{3}$/]
      }
      const trueSchema = {
        hello: ['schema', nested]
      }
      const obj = {
        hello: {
          name: 'CSC108'
        }
      }

      utils.followsSchema(trueSchema, obj).should.be.true()
    })

  it('should work on \'smaller\' types',
    function () {
      const trueSchema = {
        hello: 'string',
        world: 'string'
      }
      const obj = {
        world: '1'
      }

      utils.followsSchema(trueSchema, obj).should.be.true()
    })

  it('should break on \'larger\' types',
    function () {
      const trueSchema = {
        hello: 'string'
      }
      const obj = {
        hello: 'abc',
        world: '123'
      }

      utils.followsSchema(trueSchema, obj).should.be.false()
    })
})
