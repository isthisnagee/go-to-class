// TODO: a lot of these can be abstracted into little tiny functions
const keyFollowsSchema = module.exports.keyFollowsSchema = (schema, obj, key) => {
  if (_empty(schema) || _empty(obj)) {
    return false
  }
  const _isArray = (o) => o instanceof Array

  // check if this `key` exists in the schema
  if (!hasOwnProperty.call(schema, key)) {
    return false
  }

  // grab the type for this key
  const type = schema[key]
  // grab the value for this key
  const val = obj[key]

  // { key: 'number' }
  const typeNum = type === 'number'

  // { key: 'string' }
  const typeString = type === 'string'

  // { key: [a, b] }, a is a string
  const typeComplex = _isArray(type) && typeof type[0] === 'string'

  if (typeNum) {
    return (typeof val === 'number')
  }
  if (typeString) {
    return (typeof val === 'string')
  }

  // `val` should follow a more complex type, [a, b]
  if (typeComplex) {
    // schema has type ['schema', b], b schema
    const isSchema = type[0] === 'schema'

    // schema has type ['class', b], b object
    const isClass = type[0] === 'class'

    // schema has type ['array', b], b type of values in array
    const isArray = type[0] === 'array'

    // schema has type ['string', b], b regex
    const isString = type[0] === 'string'

    if (isString) {
      // expected a string, but `val` is not
      if (!(typeof val === 'string')) {
        return false
      }

      // check that b in ['string', b] is a regular expression
      if (!(type[1] instanceof RegExp)) {
        return false
      }

      // check that the string matches the pattern for b
      if (!val.match(type[1])) {
        return false
      }

      return true
    }

    if (isClass) {
      return (val instanceof type[1])
    }

    if (isSchema) {
      return followsSchema(type[1], val)
    }

    // schema has type ['array', b]
    if (isArray) {
      if (!(val instanceof Array)) {
        return false
      }

      // schema has type ['array', 'number']
      const isArrayOfNumber = type[1] === 'number'

      // schema has type ['array', 'string']
      const isArrayOfString = type[1] === 'number'

      // ::helper:: b is an array, schema has type [a, [c, d]]
      const isComplexArray = _isArray(type[1])

      if (isArrayOfNumber) {
        for (let i = 0; i < val.length; i++) {
          if (!(typeof val[i] === 'number')) {
            return false
          }
        }
        return true
      }

      if (isArrayOfString) {
        for (let i = 0; i < val.length; i++) {
          if (!(typeof val[i] === 'string')) {
            return false
          }
        }
        return true
      }

      if (isComplexArray) {
        // schema has type ['array', ['schema', d]], d schema
        const isArrayofSchema = type[1][0] === 'schema'

        // schema has type ['array', ['string', d]], d regex
        const isArrayofValidatedStrings = type[1][0] === 'string'

        // schema has type ['array', ['class', d]], d object
        const isArrayofClass = type[1][0] === 'class'

        if (isArrayofSchema) {
          // grab the schema it should follow
          const shouldFollow = type[1][1]

          // make sure that every element in the array follows the schema
          for (let i = 0; i < val.length; i++) {
            if (!followsSchema(shouldFollow, val[i])) {
              return false
            }
          }

          return true
        }

        // type is ['list', ['string', d]]
        if (isArrayofValidatedStrings) {
          const pattern = type[1][1]
          // check that d is a RegExp
          if (!(pattern instanceof RegExp)) {
            return false
          }

          // make sure each value at each index of `val`
          // matches the criteria
          for (let i = 0; i < val.length; i++) {
            // check that this is a string
            if (!(typeof val[i] === 'string')) {
              return false
            }
            // check that the string matches
            if (!(val[i].match(pattern))) {
              return false
            }
          }
          return true
        }

        if (isArrayofClass) {
          const checkClass = type[1][1]
          for (let i = 0; i < val.length; i++) {
            if (!(val[i] instanceof checkClass)) {
              return false
            }
          }
          return true
        }
        // unknown type of c, where c ['array', [c,d]]
        return false
      }
      // unknown type of b, where b ['array', b]
      return false
    }
    // unknonw type of a, where a [a,b]
    return false
  }
  // unknown type
  return false
}

const _empty = module.exports.isEmpty = (obj) => {
  if (obj == null) {
    return true
  }

  if (obj.length > 0) {
    return false
  }

  if (obj.length === 0) {
    return true
  }

  if (typeof obj !== 'object') {
    return true
  }

  for (var key in obj) {
    if (hasOwnProperty.call(obj, key)) {
      return false
    }
  }

  // we don't know 0_0
  return true
}

/*
 * an object follows a schema iff all of its keys exist
 * in a schema and follow the types. So an object need not
 * have all the keys in the schema.
 */
const followsSchema = module.exports.followsSchema = (schema, obj) => {
  for (var key in obj) {
    if (!keyFollowsSchema(schema, obj, key)) {
      return false
    }
  }
  return !_empty(obj) || false
}
