const courseSchema = {
  name: ['string', /[A-Z]{3}[0-9]{3}/]
}

const winnerSchema = {
  term: 'string',
  user: 'string'
}

const verifyMissedCourseSchema = {
  name: 'string',
  _id: 'number',
  date: ['class', Date],
  courseCode: ['schema', courseSchema]
}

const missedCourseSchema = {
  courseCode: ['schema', courseSchema],
  missed: 'number'
}

const userSchema = {
  _id: 'string',
  name: 'string',
  username: 'string',
  courses: ['array', ['schema', courseSchema]],
  numMissedCourses: 'number',
  verifyMissed: ['array', ['schema', verifyMissedCourseSchema]],
  distMissedCourses: ['array', ['schema', missedCourseSchema]]
}

const groupSchema = {
  name: 'string',
  _id: 'string',
  // a list of usernames
  users: ['array', ['string', /^[A-Za-z0-9_-]{5-25}$/]],
  admin: ['schema', userSchema],
  winners: ['array', ['schema', winnerSchema]]
}

module.exports = {
  winnder: winnerSchema,
  course: courseSchema,
  verifyMissedCourse: verifyMissedCourseSchema,
  missedCourse: missedCourseSchema,
  user: userSchema,
  group: groupSchema
}
