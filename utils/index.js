const wrap = require('co-monk')
const monk = require('monk')

const schema = require('./schema.js')
const validator = require('./validator.js')

const db = monk('localhost/27017')
const users = wrap(db.get('users'))
const groups = wrap(db.get('groups'))

function *logger (next) {
  const start = new Date()

  yield next

  const ms = new Date() - start
  console.log('%s %s - %s', this.method, this.url, ms)
}

function *getUser (username) {
  let user = yield users.findOne({ username: username })
  if (!user) {
    // create the user
    users.insert({ username: username })
  }

  return user
}

/*
 * Filters out all key:val in `obj` that match a schema type.
 */
function schemaFilterBy (typeSchema, obj) {
  let newObj = {}
  for (let key in obj) {
    if (validator.keyFollowsSchema(typeSchema, obj, key)) {
      newObj[key] = obj[key]
    }
  }
  return newObj
}

module.exports = {
  /* general */
  logger: logger,
  /* db utils */
  users: users,
  groups: groups,
  getUser: getUser,
  /* schema utils */
  schema: schema,
  followsSchema: validator.followsSchema,
  schemaFilterBy: schemaFilterBy,
  keyFollowsSchema: validator.keyFollowsSchema,
  isEmpty: validator.isEmpty
}
