const parse = require('co-body')

const utils = require('../utils')

const users = utils.users
const schemaFilterBy = utils.schemaFilterBy
const userSchema = utils.schema.user

module.exports = {
  get: get,
  add: add,
  update: update,
  remove: remove
}

/*
 * Returns the user with the given id as the body
 */
function *get (username) {
  // look up the user
  const user = yield users.findOne({ username: username })
  if (user) {
    this.body = user
    this.status = 200
  } else {
    this.body = {}
    this.status = 404
  }
}

/*
 * Adds a user, filtering out data that does not
 * match the user schema
 */
function *add () {
  // parse user
  const data = yield parse(this)

  // check that username is part of the data
  if (data.username === undefined) {
    this.status = 404
    return
  }

  // check that the username is unique
  const numUsername = yield users.count({ username: data.username })
  if (numUsername > 0) {
    this.status = 405
    return
  }

  // remove any _id that may have been passed
  delete data._id

  const filteredData = schemaFilterBy(userSchema, data)

  if (utils.isEmpty(filteredData)) {
    this.status = 404
  } else {
    // store in db
    const newUser = yield users.insert(filteredData)

    // return location of users and http 'ok'
    this.set('location', `/user/${newUser.username}`)
    this.status = 201
    this.body = newUser
  }
}

/*
 * Update a user with username `username`, filtering out bad data
 */
function *update (username) {
  // get the user
  const user = yield users.findOne({username: username})

  // get the new data
  const data = yield parse(this)

  // grab only valid data
  const validData = schemaFilterBy(userSchema, data)

  // make sure an id isn't passed
  delete validData._id

  // remove any reference to username
  delete validData.username

  if (utils.isEmpty(validData)) {
    this.body = {}
    this.status = 304
  } else {
    yield users.update(user, {$set: validData})
    const updatedUser = yield users.findOne({username: username})
    // update user with the valid data
    this.body = updatedUser
    this.status = 200
  }
}

/*
 * Delete the user with the given `id`. We return 200 even
 * when the user does not exist.
 * TODO: return 404 if user doesn't exist?
 */
function *remove (username) {
  yield users.remove({ username: username })
  this.status = 200
}
