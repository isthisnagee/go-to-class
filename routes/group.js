const parse = require('co-body')

const utils = require('../utils')

const groups = utils.groups
const users = utils.users

const groupSchema = utils.schema.group

module.exports = {
  get: get,
  add: add,
  update: update,
  remove: remove,
  top: top
}

function *top (name) {
  const group = yield groups.findOne({ name: name })
  if (group.users === undefined) {
    this.body = []
    this.result = 404
  }
  // get all the users
  let groupMembers = []
  for (let i = 0; i < group.users.length; i++) {
    groupMembers.push(yield users.findOne({ username: group.users[i] }))
  }

  // get the smallest number of courses missed
  const minMissed = Math.min.apply(Math,
    groupMembers.map(m => m.numMissedCourses || 0))

  // get the users with the least amount of courses missed
  const topMembers = groupMembers.filter(m => m.numMissedCourses === minMissed)

  this.body = topMembers
  this.result = 200
}

function *get (name) {
  // look up the group
  const group = yield groups.findOne({ name: name })
  if (group) {
    this.body = group
    this.status = 200
  } else {
    this.body = {}
    this.status = 404
  }
}

function *add () {
  // parse group
  const data = yield parse(this)
  const filteredData = utils.schemaFilterBy(groupSchema, data)
  if (utils.isEmpty(filteredData)) {
    this.status = 404 // mothod not allowed
    return
  }
  // make sure the name field is given
  if (filteredData.name === undefined) {
    this.status = 405
    return
  }

  // check that this does not exist already
  const numWithName = yield groups.count({ name: filteredData.name })

  if (numWithName > 0) {
    this.status = 405
    return
  }

  // make sure no id is passed
  delete filteredData._id

  const newGroup = yield groups.insert(filteredData)

  this.status = 201
  this.set('location', `/group/${newGroup.name}`)
  this.body = newGroup
}

function *update (name) {
  // get the group
  const group = yield groups.findOne({
    name: name
  })

  // validate the group
  if (group === undefined || utils.isEmpty(group)) {
    this.status = 404
    return
  }

  // get the data
  const data = yield parse(this)

  // get "clean" data
  const validData = utils.schemaFilterBy(groupSchema, data)

  // make sure there is data to insert
  if (utils.isEmpty(validData)) {
    this.status = 404
    return
  }

  // names cannot be changed
  delete validData.name

  // id's cannot be changed
  delete validData._id

  yield groups.update(group, {$set: validData})

  const updatedGroup = yield groups.findOne({
    name: name
  })

  this.body = updatedGroup
  this.status = 200
}

function *remove (name) {
  yield groups.remove({ name: name })
  this.status = 200
}
