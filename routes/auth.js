const userauth = require('koa-userauth')
// const users = require('utils').users

let auth = userauth({
  match: '/login',
  // auth system login url
  loginURLFormatter: function (url) {
    return url + '/no'
  },
  // login callback and getUser info handler
  getUser: function* () {
    const token = this.query.token
    console.log(token)
    // get user
    return 'Nagee'
  }
})

module.exports = auth
